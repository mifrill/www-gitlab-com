title: GitLab for Open Source
description: Open Source organizations can access our top tier features, plus
  50,000 CI pipeline minutes, for free. Find out more!
file_name: open-source
twitter_image: /images/opengraph/open-source-card.png
header:
  image: /images/icons/explore-open-source.svg
  tagline: Open Source organizations can access our top tier features, plus 50,000
    CI pipeline minutes, for free.
  title: GitLab for Open Source
  buttons:
    - button_url: /solutions/open-source/join/
      button_text: Join the GitLab for Open Source program
  subtitle: GitLab believes in a world where everyone can contribute and we like
    to support those who share our mission
tracks:
  card:
    - heading: Grow your FOSS project
      subtitle: Use a single, user-friendly platform
      body: >-
        GitLab helps you onboard newcomers to your project faster and more
        easily than ever with a single tool for all aspects of your software
        development lifecycle.


        Not only created for developers, GitLab enables cross-functional team collaboration. It's a perfect tool for Design, Documentaion, Engagement, and Governance teams alike. GitLab's user-friendly and inclusive design makes it easier to attract and retain contributors from diverse backgrounds to help your open source project thrive.
      icon_full: /images/icons/slp-cicd.svg
    - heading: World-class CI/CD and Security
      body: >-
        Continuous Integration (CI) and Continuous Delivery (CD) help you cut
        down on manual work and increase automation. That way, you can build and
        deploy great quality software more quickly.


        Since [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) is fully integrated, it allows you to get set up faster and provides a seamless experience. GitLab for Open Source members get 50,000 CI pipeline minutes per month for free.


        Using GitLab CI with one of our top tiers provides you with best-in-class functionality. However, if your FOSS community is using another source code management platform, we still encourage you to use GitLab CI!


        With [Security baked into our CI pipelines](https://about.gitlab.com/solutions/dev-sec-ops/), you get a robust set of application security scans. All of the results are provided for development teams in the Merge Request view, and can be managed in the security dashboard by security teams.


        Still deciding? We encourage you to [learn more about how GitLab CI compares](https://about.gitlab.com/devops-tools/) to other popular CI providers such as Travis CI and Circle CI and see for yourself.
      icon_full: /images/icons/slp-lock.svg
    - heading: Choose the GitLab distribution that's right for your FOSS community
      body: >-
        You can choose to host your open source project on GitLab.com or
        self-host your own instance. Each of these options has [powerful
        features](https://about.gitlab.com/features/) to enable you to develop
        enterprise-grade software at scale.


        In alignment with our [Transparency value](https://about.gitlab.com/handbook/values/#transparency), all of GitLab's code is source-available and the open source components of GitLab are published under an [MIT open source license](https://opensource.org/licenses/MIT). The [open source distribution](https://about.gitlab.com/install/ce-or-ee/) is [available to dowload here](https://about.gitlab.com/install/?version=ce) and contains all the same features as our Free tier.


        [GitLab's Open Source Partners](https://about.gitlab.com/solutions/open-source/partners/), are leveraging GitLab's top tier in both SaaS and self-managed instances, as well as leveraging the power of GitLab's open source distribution. They serve as examples of how large open source communities can thrive with GitLab.


        If you have any questions, please reach out to us via opensource@gitlab.com.
      icon_full: /images/icons/slp-agile.svg
    - heading: "FOSS Spotlight: GNOME"
      body: <iframe width="560" height="315"
        src="https://www.youtube-nocookie.com/embed/v6GTrbfe9xk" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media;
        gyroscope; picture-in-picture" allowfullscreen></iframe>
      icon_full: /images/icons/slp-sourcecode.svg
  heading: About
  body: >-
    GitLab exists today in large part thanks to the work of a vast community of
    open source contributors around the world. As a company, we are deeply
    committed to being a [good steward of the GitLab Open Source
    project](https://about.gitlab.com/company/stewardship/).


    To give back to this community who gives us so much, we want to enable teams be more efficient, secure, and productive. We believe the best way for them to achieve this is by using as many of the capabilities of GitLab as possible.


    We offer our top tiers, plus 50,000 CI pipeline minutes per month, for free to qualifying open source projects and organizations.
block_list_1:
  heading: Resources
  subheading: We think there’s something magical about building free software with
    free software.
  items:
    - text: Community Resources
      href: /community
      icon: /images/icons/solid-icons/community-icon.svg
    - text: Trending Projects
      href: https://gitlab.com/explore/projects/starred
      icon: /images/icons/solid-icons/trending-icon.svg
    - text: GitLab Development Kit
      href: https://gitlab.com/gitlab-org/gitlab-development-kit/
      icon: /images/downloads/logo.svg
    - text: Why we switched to DCO
      href: /blog/2017/11/01/gitlab-switches-to-dco-license
      icon: /images/icons/solid-icons/docs-icon.svg
partners:
  heading: Open Source Partners
  type: open-source
customer_quotes:
  - blockquote: Free software enables and empowers us as a society to build upon past knowledge and create greater and more impactful technologies.
    attribution: Carlos Soriano
    attribution_title: Board of Directors, GNOME
